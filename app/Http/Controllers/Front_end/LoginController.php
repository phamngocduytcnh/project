<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginMemberRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showLoginForm(){
    	return view('front-end.auth.login');
    }
    public function memberLogin(LoginMemberRequest $request){
    	$data = [
    		'email' => $request->email,
    		'password' => $request->password,
    		'level' => 0
    	];
    	$remember = $request->has('remember') ? true : false;
    	if(Auth::attempt($data, $remember)){
    		return redirect()->intended('blog/list');
    	}
    	else{
    		return back()->with(['error'=>'Login is failled']);
    	}
    }
    public function memberLogout(Request $request){
        Auth::logout();
        $request->session()->flush();
        return redirect('member/login');
    }
}
