<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Socialite;
use Auth;
use Exception;
use App\User;
use File; 

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function sendFailledResponse(){
        return redirect()->route('member.login')->with(['error' => 'Unable to login, try with another provider to login.']);
    }
    public function sendSuccessResponse(){
        return redirect('/index');
    }
    public function callback($provider)
    {
    	try 
    	{
            //Kiểm tra user có tồn tại hay không
    		$user = Socialite::driver($provider)->user();
    		$finduser = User::where('email', $user->getEmail())->first();
            //Nếu user tồn tại
    		if($finduser)
    		{
                //Update provider, provider_id và access_token nếu như trước đó chưa login with provider
                //Login bằng email và password
                $finduser->provider = $provider;
                $finduser->provider_id = $user->getId();
                $finduser->access_token = $user->token;
                $finduser->save();
                //$img = $user->getAvatar();
                //Login with remember_token
    			Auth::login($finduser, true);
    			return $this->sendSuccessResponse();
    		}
    		else
    		{
                //Kiểm tra provider account có email hay không, nếu có thì tạo một user mới
                if($user->getEmail()){
                    $newUser = User::create([
                        'name' => $user->getName(),
                        'email' => $user->getEmail(),
                        'provider' => $provider,
                        'provider_id'=> $user->getId(),
                        'password' => Hash::make('12345678'),
                        'level' => 0,
                        'access_token' => $user->token,
                        //'img' => $user->getAvatar();
                        //$user->getNickname()
                    ]);
                    //Login with remember_token
                    Auth::login($newUser,true);
                    return $this->sendSuccessResponse();
                }
                else
                {
                    //Nếu như provider account không có e-mail:
                   return $this->sendFailledResponse();
                }
    			
    		}
    	}
    	catch (Exception $e) {
            return $this->sendFailledResponse();
        }

    }
}
