<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Rate;
use App\User;

class BlogController extends Controller
{
    public function list(){
        $blogs = Blog::paginate(config('frontend.pagenation'));
        return view('front-end.blogs.index', compact('blogs'));
    }
    public function show($id){
    	$blog = Blog::findOrFail($id);
    	$count = Blog:: count();
    	$comments = Comment::where('blog_id', $id)->get();
        $avg_rate = Rate::where('blog_id', $id)->avg('rate');
        if( ($avg_rate - floor($avg_rate)) >= 0.5 ){
            $avg_rate = round($avg_rate);
        }
        else {
            $avg_rate = floor($avg_rate);
        }
        $count_rate = Rate::where('blog_id', $id)->count();
    	return view('front-end.blogs.single', compact('blog','count','id','comments','avg_rate','count_rate'));
    }
    public function comment(Request $request, $id){
    	$comment = Comment::create([
            'content' => $request->content,
            'member_id' => Auth::user()->id,
            'blog_id' => (int) $id,
            'id_comment' => 0
        ]);
    	$name = $comment->Member['name'];
    	$img = asset('uploads/profiles/'.Auth::user()->img);
        return response()->json(['comment'=> $comment, 'name'=>$name, 'img'=> $img]);  
    }
    public function sub_comment(Request $request, $id){
        $comment = Comment::create([
            'content' => $request->content,
            'member_id' => Auth::user()->id,
            'blog_id' => (int) $id,
            'id_comment' => $request->comment_id
        ]);
    	$name = $comment->Member['name'];
        $img = asset('uploads/profiles/'.Auth::user()->img);
    	return response()->json(['comment'=> $comment, 'name'=>$name, 'img'=> $img]); 
    }
    public function rating(Request $request, $id){
        $rate = Rate::create([
            'blog_id' => (int) $id,
            'member_id' => Auth::user()->id,
            'rate' => (int) $request->rate,
        ]);
        $avg_rate = Rate::where('blog_id', $id)->avg('rate');
        if( ($avg_rate - floor($avg_rate)) >= 0.5 ){
            $avg_rate = round($avg_rate);
        }
        else {
            $avg_rate = floor($avg_rate);
        }
        $count_rate = Rate::where('blog_id', $id)->count();
        return response()->json(['avg_rate'=> $avg_rate, 'count_rate' => $count_rate]);
    }
}
