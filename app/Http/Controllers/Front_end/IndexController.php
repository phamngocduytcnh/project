<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB; 

class IndexController extends Controller
{
    //
    public function showIndex()
    {
    	$products = Product::orderBy('created_at', 'desc')->limit(6)->get();
    	return view('front-end.index.index', compact('products'));
    }

    public function sendSuccessResponse($cart, $request)
    {
        $request->session()->put('cart', $cart);
        return response()->json(['message' => 'Add product to cart is successfull !']); 
    }

    public function sendFailledResponse()
    {
        return response()->json(['message' => 'Add product to cart is failled !']); 
    }

    public function addToCart(Request $request)
    {
    	$id = $request->productId;
        $product = Product::findOrFail( (int)$id );
    	$cart = $request->session()->get('cart');
    	if(empty($cart))
    	{
            $cart = [
                $id => [
                    'name' => $product->name,
                    'quanity' => 1,
                    'price' => $product->price * (1- $product->sale_off/100),
                    'img' => 'small_'.json_decode($product->img)[0],
                    'dir' => $product->member_id,
                ]
            ];
            return $this->sendSuccessResponse($cart, $request);
    	}

    	if(isset($cart[$id]))
        {
    		$cart[$id]['quanity']++;
    		return $this->sendSuccessResponse($cart, $request);
    	}

        $cart[$id] = [
            'name' => $product->name,
            'quanity' => 1,
            'price' => $product->price * (1- $product->sale_off/100),
            'img' => 'small_'.json_decode($product->img)[0],
            'dir' => $product->member_id,
        ];
        return $this->sendSuccessResponse($cart, $request);
    }
    public function priceRange(Request $request){
        $min = $request->min;
        $max = $request->max;
        $products = DB::table('products')->whereRaw('price * (1-sale_off/100) > ?', $min)
                                        ->whereRaw('price * (1-sale_off/100) < ?', $max)
                                        ->get();
        $array = [];
        foreach ($products as $product) {
            $data = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'sale_off' =>  $product->sale_off,
                'img' => asset('uploads/products/'.$product->member_id.'/'.json_decode($product->img)[0] ) ,
            ];
            $array[] = $data;
        }
        return response()->json(['array' => $array]);
    }
    public function searchIndex(Request $request){
        $search = '%'.$request->search.'%';
        $products = Product::where('name','like',$search)->get();
        $array = [];
        foreach ($products as $product) {
            $data = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'sale_off' =>  $product->sale_off,
                'img' => asset('uploads/products/'.$product->member_id.'/'.json_decode($product->img)[0] ) ,
            ];
            $array[] = $data;
        }
        return response()->json(['array' => $array]);
    }
}
