<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    //
	public function showProduct($id)
	{
		$product = Product::findOrFail($id);
		$arrayImg = json_decode($product->img);
		return view('front-end.products.product-detail', compact('product','arrayImg'));
	}

}
