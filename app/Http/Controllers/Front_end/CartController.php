<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
class CartController extends Controller
{
    //
    public function showCart(Request $request){
    	$total = 0;
    	$cart = $request->session()->get('cart');
        if(isset($cart) && !empty($cart) )
        {
            foreach ($cart as  $item) {
                $total += $item['price'] * $item['quanity'];
            }
        }
    	return view('front-end.cart.cart', compact('cart','total'));
    }
    public function actionCart(Request $request){
    	$id = $request->id;
    	$action = $request->action;
    	$cart = $request->session()->get('cart');
    	//Action : up
    	if($action == 'up')
    	{
    		if(isset($cart[$id]) && !empty($cart[$id]) )
    		{
	    		$cart[$id]['quanity']++;
	    		$request->session()->put('cart', $cart);
	    		return response()->json(['cart' => $cart, 'quanity' => $cart[$id]['quanity'], 'price' => $cart[$id]['price'] ]);
    		}
    	}
    	//Action down: if quanity = 0 --> Delete
    	if( $action == 'down')
    	{
    		if(isset($cart[$id]) && !empty($cart[$id]) )
    		{
	    		$cart[$id]['quanity']--;
	    		if($cart[$id]['quanity'] == 0)
    			{
    				foreach ($cart as $key => $value) {
    					if($key == $id){
    						$action = 'remove';
    						$quanity = $cart[$id]['quanity'];
    						$price = $cart[$id]['price'];
    						unset($cart[$id]);
    						$request->session()->put('cart', $cart);
    						return response()->json(['cart' => $cart,'quanity' => $quanity , 'action' => $action, 'price' => $price]);
    					}
    				}
    			}
	    		$request->session()->put('cart', $cart);
	    		return response()->json(['cart' => $cart, 'quanity' => $cart[$id]['quanity'], 'action' => $action, 'price' => $cart[$id]['price']]);
    		}
    	}
    	//Action: Delete
    	if( $action == 'delete')
    	{
    		if(isset($cart[$id]) && !empty($cart[$id]) )
    		{
				foreach ($cart as $key => $value) {
					if($key == $id){
						$action = 'remove';
						$quanity = $cart[$id]['quanity'];
						$price = $cart[$id]['price'];
						unset($cart[$id]);
						$request->session()->put('cart', $cart);
						return response()->json(['cart' => $cart, 'quanity' => $quanity, 'price' => $price]);
					}
				}
    			
    		}
    	}
    }
}
