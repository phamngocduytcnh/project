<?php

namespace App\Http\Controllers\Admin_api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class BlogController extends Controller
{
    public function list(){
    	$client = new \GuzzleHttp\Client();
	    $request = $client->get('http://localhost:81/api/blog');
	    $response = $request->getBody()->getContents();
	    $value = json_decode($response, true);
	    $myCollectionObj = collect($value['blog']);
	    $data = $this->paginate($myCollectionObj);
        $data->withPath('get-api');
	    return view('Admin-api.blog.list', compact('data'));
    }
    public function paginate($items, $perPage = 2, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
