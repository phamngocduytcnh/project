<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $table = 'blogs';
    protected $fillable = [
    	'id', 'title','description','content','userId', 'created_at'
    ];
    public $timestamps = true;
    public function User(){
    	return $this->hasOne('App\User','id','userId');
    }
    
}
