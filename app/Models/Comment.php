<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'comments';
    protected $fillable = [
    	'id', 'content', 'member_id','created_at','blog_id','id_comment'
    ];
    public $timestamps = true;
    public function Member(){
    	return $this->hasOne('App\User','id','member_id');
    }
}
