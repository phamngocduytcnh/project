<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $fillable = [
    	'id', 'name','price','img','member_id','category_id', 'brand_id','state_id','sale_off','message','created_at'
    ];
    public $timestamps = true;
    public function Category(){
    	return $this->hasOne('App\Models\Category','id','category_id');
    }
    public function Brand(){
    	return $this->hasOne('App\Models\Brand','id','brand_id');
    }
    public function State(){
    	return $this->hasOne('App\Models\State','id','state_id');
    }
}
