<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '848134198464-dctp80ej3rs01vp22f1gmet8k4cun9su.apps.googleusercontent.com',
        'client_secret' => 'kTcZWKYT9FWD010TY1vwTYtb',
        'redirect' => 'http://localhost:81/member/login/google/callback',
    ],

    'facebook' => [
     'client_id' => '717183122424163',
     'client_secret' => '88bd087631f728b379953922890ee134',
     'redirect' => 'http://localhost:81/member/login/facebook/callback',
   ], 
];
