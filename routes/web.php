<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group([
	'namespace' => 'Front_end'
], function(){
	Route::get('blog/list', 'BlogController@list')->name('blog-list');
	Route::get('blog/list/{id}', 'BlogController@show');
	Route::get('product/list/{id}','ProductController@showProduct');
	Route::get('/','IndexController@showIndex')->name('index');
	Route::post('ajaxPriceRange', 'IndexController@priceRange');
	Route::post('ajaxAddProduct', 'IndexController@addToCart');
	Route::post('ajaxSearchIndex', 'IndexController@searchIndex');
	Route::get('cart','CartController@showCart')->name('cart');
	Route::post('ajaxActionCart', 'CartController@actionCart');
	Route::get('checkout','CheckOutController@show');
	Route::get('cart-mail','CheckOutController@sendMail');
	Route::get('search', 'SearchController@show');
	Route::post('ajaxSearchAdvance','SearchController@searchAdvance');


	Route::group([
		'prefix'=> 'member',
		'middleware'=> ['not.member']
	], function(){
		Route::get('/login', 'LoginController@showLoginForm')->name('member.login');
		Route::post('/login', 'LoginController@memberLogin');
		Route::get('/login/{provider}', 'SocialController@redirect');
		Route::get('/login/{provider}/callback','SocialController@callback');
		Route::get('/register', 'RegisterController@showRegisterForm')->name('member.register');
		Route::post('/register', 'RegisterController@register');
	});

	Route::group(['middleware'=> ['member']], function(){
		Route::post('member/logout', 'LoginController@memberLogout');
		Route::post('blog/list/{id}/comment', 'BlogController@comment');
		Route::post('blog/list/{id}/sub_comment', 'BlogController@sub_comment');
		Route::post('blog/list/{id}/rating', 'BlogController@rating');
		Route::get('member/account/profile','AccountController@showProfile')->name('member.profile');
		Route::post('member/account/profile','AccountController@updateProfile');
		Route::get('member/account/product/create','AccountController@formAddProduct')->name('member.createProduct');
		Route::post('member/account/product/create','AccountController@addProduct');
		Route::get('member/account/product/list','AccountController@listProduct')->name('member.listProduct');
		Route::get('member/account/product/list/{id}/edit','AccountController@editFormProduct');
		Route::post('member/account/product/list/{id}/edit','AccountController@editProduct');
		Route::get('member/account/product/list/{id}/delete','AccountController@deleteProduct');
	});
});

Auth::routes();

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Auth'	
], function(){
	Route::get('/', 'LoginController@showLoginForm');
	Route::get('/login', 'LoginController@showLoginForm');
	Route::post('/login','LoginController@login');
	Route::get('/register', 'RegisterController@showRegistrationForm');
	Route::post('/register', 'RegisterController@register');
	Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin_api',
	'middleware' => ['admin']
], function(){
	Route::get('get-api','BlogController@list');
});

Route::group([
	'prefix'=> 'admin',
	'namespace' => 'Admin',
	'middleware'=> ['admin']
], function(){
	Route::get('/index', 'IndexController@index')->name('admin.index');
	Route::get('/table', 'TableController@index')->name('table');
	Route::get('/profile', 'UserController@index')->name('profile');
	Route::post('/profile', 'UserController@updateProfile');
	Route::get('/country', 'CountryController@index')->name('country');
	Route::post('/country', 'CountryController@addCountry');
	//Blog-Add new blog:
	Route::get('/blog/add', 'BlogController@add')->name('add-blog');
	Route::post('/blog/add', 'BlogController@create');
	//Blog-List of Blog:
	Route::get('/blog/list', 'BlogController@list')->name('list-blog');
	

	Route::get('/blog/edit/{id}', 'BlogController@show')->name('show');
	Route::post('/blog/edit/{id}', 'BlogController@edit');

	Route::get('/blog/delete/{id}', 'BlogController@delete');
	Route::get('/blog/index', 'BlogController@index');

});
