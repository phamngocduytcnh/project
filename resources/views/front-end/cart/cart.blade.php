@extends('front-end.layouts.main')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@if(session('cart'))
							@foreach(session('cart') as $key => $cart)
							<tr id="{{$key}}">
								<td class="cart_product">
									<a href=""><img src="{{asset('uploads/products/'.$cart['dir'].'/'.$cart['img'].'')}}" alt=""></a>
								</td>
								<td class="cart_description">
									<h4><a href="">{{$cart['name']}}</a></h4>
									<p>Web ID: 1089772</p>
								</td>
								<td class="cart_price">
									<p>$ {{$cart['price']}}</p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<a class="cart_quantity_up" href=""> + </a>
										<input class="cart_quantity_input" type="text" name="quantity" value="{{$cart['quanity']}}" autocomplete="off" size="2">
										<a class="cart_quantity_down" href=""> - </a>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">$ {{$cart['quanity'] * $cart['price']}}</p>
								</td>
								<td class="cart_delete">
									<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
								</td>
							</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="{{url('checkout')}}">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span></span></li>
							<li>Eco Tax <span></span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>$ <span id="total">{{ $total }}</span></span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
			$("a.cart_quantity_up").click(function(e){
				e.preventDefault();
				var tr = $(this).closest("tr");
				var id = tr.attr('id');
				var action = 'up';
				var total = parseFloat($('#total').text());
				$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxActionCart')}}",
		    		data : {
		    			'id' : id,
		    			'action': action,
		    		},
		    		success:function(data){
		    			console.log(data.cart);
		    			$(tr).find('input.cart_quantity_input').val(data.quanity);
		    			total += data.price;
		    			$('#total').text(total);
		    		}
		    	});
				
			})
			$("a.cart_quantity_down").click(function(e){
				e.preventDefault();
				var tr = $(this).closest("tr");
				var id = tr.attr('id');
				var action = 'down';
				var total = parseFloat($('#total').text());
				$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxActionCart')}}",
		    		data : {
		    			'id' : id,
		    			'action': action,
		    		},
		    		success:function(data){
		    			console.log(data.cart);
		    			if(data.action == 'down')
		    			{
		    				$(tr).find('input.cart_quantity_input').val(data.quanity);
		    			}
		    			if(data.action == 'remove')
		    			{
		    				tr.remove();
		    			}
		    			total -= data.price;
		    			$('#total').text(total);
		    		}
		    	});
				
			})
			$("a.cart_quantity_delete").click(function(e){
				e.preventDefault();
				var tr = $(this).closest("tr");
				var id = tr.attr('id');
				var action = 'delete';
				var total = parseFloat($('#total').text());
				$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxActionCart')}}",
		    		data : {
		    			'id' : id,
		    			'action': action,
		    		},
		    		success:function(data){
		    			console.log(data.cart);
		    			tr.remove();
		    			total -= data.price * data.quanity;
		    			$('#total').text(total);
		    		}
		    	});
				
			})
		})
	</script>
@endsection