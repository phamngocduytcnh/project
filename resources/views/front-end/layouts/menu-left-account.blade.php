				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Menu</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="{{route('member.profile')}}">Profile</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#product">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Product
										</a>
									</h4>
								</div>
								<div id="product" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="{{route('member.createProduct')}}">Create new product</a></li>
											<li><a href="{{route('member.listProduct')}}">List of product</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div><!--/category-products-->
					</div>
				</div>