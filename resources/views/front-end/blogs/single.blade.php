@extends('front-end.layouts.main')
@section('content')
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{ $blog->title}} </h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i>{{$blog->User['name']}}</li>
									<li><i class="fa fa-clock-o"></i> {{ date_format( $blog->created_at, 'h-i-sa')  }}</li>
									<li><i class="fa fa-calendar"></i> {{ date_format( $blog->created_at, 'M-d-Y')  }}</li>
								</ul>
								<div class="rate">
					                <div class="vote">
					                    <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
					                    <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
					                    <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
					                    <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
					                    <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
					                    <span id="error_rate"></span>
					                </div> 
            					</div>
							</div>
							<p>{{$blog->description}}</p>
							<!--Panation -->
							{!! $blog->content !!}
							<!--End panation -->

							<!--Pre and Next button -->
							@if ( $id <= 1 )
								<div class="pager-area">
									<ul class="pager pull-right">
										<li><a href="{{url('blog/list/'.$count)}}">Pre </a></li>
										<li><a href="{{url('blog/list/'. ($id + 1) )}}">Next </a></li>
									</ul>
								</div>
							@elseif ( $id > ($count-1))
								<div class="pager-area">
									<ul class="pager pull-right">
										<li><a href="{{url('blog/list/'. ($id - 1) )}}">Pre </a></li>
										<li><a href="{{url('blog/list/1')}}">Next </a></li>
									</ul>
								</div>
							@else 
								<div class="pager-area">
									<ul class="pager pull-right">
										<li><a href="{{url('blog/list/'. ($id - 1) )}}">Pre </a></li>
										<li><a href="{{url('blog/list/'. ($id + 1) )}}">Next </a></li>
									</ul>
								</div>
							@endif
							<!--End Pre and Next button -->
						</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						<ul class="ratings">
							<li class="rate-this">Rating this item:</li>
							<li id="show-rate">
								@isset($avg_rate)
									@for($i=1; $i<= 5; $i++)
										@if($avg_rate >= $i)
											<i class="fa fa-star color"></i>
										@else
											<i class="fa fa-star"></i>
										@endif
									@endfor
								@endisset
							</li>
							<li id="show-count" class="color">({{$count_rate}} votes)</li>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="{{asset('front-end/images/blog/socials.png')}}" alt=""></a>
					</div><!--/socials-share-->

					<!--Comments-->
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						@if(isset($comments) && !empty($comments))
						<ul class="media-list">
							@foreach ($comments as $comment)
								@if($comment->id_comment == 0)
								<li class="media" id="{{$comment->id}}">
									<a class="pull-left" href="#">
										<img class="media-object" src="{{asset('uploads/profiles/'.$comment->Member['img'].'')}}" alt="">
									</a>
									<div class="media-body">
										<ul class="sinlge-post-meta">
											<li><i class="fa fa-user"></i>{{$comment->Member['name']}}</li>
											<li><i class="fa fa-clock-o"></i> {{ date_format( $comment->created_at, 'h-i-sa')  }}</li>
											<li><i class="fa fa-calendar"></i> {{ date_format( $comment->created_at, 'M-d-Y')  }}</li>
										</ul>
										<p>{{$comment->content}}</p>
										<a class="btn btn-primary btn-reply" id="{{$comment->id}}"  ><i class="fa fa-reply"></i>Replay</a>
									</div>
								</li>
								@endif
								@foreach ($comments as $sub_comment)
									@if($sub_comment->id_comment == $comment->id)
										<li class="media second-media">
											<a class="pull-left" href="#">
												<img class="media-object" src="{{asset('uploads/profiles/'.$sub_comment->Member['img'].'')}}" alt="">
											</a>
											<div class="media-body">
												<ul class="sinlge-post-meta">
													<li><i class="fa fa-user"></i>{{$sub_comment->Member['name']}}</li>
													<li><i class="fa fa-clock-o"></i> {{ date_format( $sub_comment->created_at, 'h-i-sa')  }}</li>
													<li><i class="fa fa-calendar"></i> {{ date_format( $sub_comment->created_at, 'M-d-Y')  }}</li>
												</ul>
												<p>{{$sub_comment->content}}</p>
												<a class="btn btn-primary" id="{{$sub_comment->id}}"  ><i class="fa fa-reply"></i>Replay</a>
											</div>
										</li>
									@endif
								@endforeach
							@endforeach
						</ul>
						@endif			
					</div><!--/Response-area-->
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-4">
								<h2>Leave a replay</h2>
								<form>
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<input type="text" placeholder="write your name...">
									<div class="blank-arrow">
										<label>Email Address</label>
									</div>
									<span>*</span>
									<input type="email" placeholder="your email address...">
									<div class="blank-arrow">
										<label>Web Site</label>
									</div>
									<input type="email" placeholder="current city...">
								</form>
							</div>
							<div class="col-sm-8">
								<div class="text-area">
									<div class="blank-arrow">
										<label id="error">Message:</label>
									</div>
									<span>*</span>
									<textarea name="comment" id="comment" rows="11"></textarea>
									<a class="btn btn-primary btn-submit" id="{{$blog->id}}" >Send comment</a>
								</div>
							</div>
						</div>
					</div><!--/Repaly Box-->
				</div>	
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function(){
			function sub_commment(data){
				var html ="<li class='media second-media'>"
  							+ "<a class='pull-left'>"
								+ "<img class='media-object' src='"+ data.img +"' alt=''>"
							+ "</a>"
							+ "<div class='media-body'>"
								+ "<ul class='sinlge-post-meta'>"
									+ "<li><i class='fa fa-user'></i>" + data.name + "</li>"
									+ "<li><i class='fa fa-clock-o'></i> </li>"
									+ "<li><i class='fa fa fa-calendar'></i> </li>"
								+ "</ul>"
								+ "<p>" + data.comment.content + "</p>"
								+ "<a class='btn btn-primary' ><i class='fa fa-reply'></i>Replay</a>"
							+ "<div"
  						+"</li>";
  				$("ul.media-list > li[id='"+ comment_id +"']").after(html);
  				$("#comment").val("");
			}
			function comment(data){
				var html ="<li class='media' id='"+ data.comment.id+"' >"
  							+"<a class='pull-left'>"
								+ "<img class='media-object' src='"+ data.img +"' alt=''>"
							+"</a>"
							+"<div class='media-body'>"
								+ "<ul class='sinlge-post-meta'>"
									+"<li><i class='fa fa-user'></i>"+ data.name +"</li>"
									+"<li><i class='fa fa-clock-o'></i> </li>"
									+"<li><i class='fa fa fa-calendar'></i> </li>"
								+"</ul>"
								+"<p>"+ data.comment.content + "</p>"
								+"<a class='btn btn-primary btn-reply' id='"+ data.comment.id +"' ><i class='fa fa-reply'></i>Replay</a>"
							+ "<div"
  						+"</li>";
  				var check = $('ul.media-list > li').length;
  				if(check == 0){
  					$('ul.media-list').html(html);
  				}
  				else{
  					$('ul.media-list >li:last-child').after(html);	
  				}
  				$("#comment").val("");
			}
			var sub_comment = false;
			var comment_id = 0;
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});
			$(document).on('click','.btn-reply', function(){
				sub_comment = true;
				comment_id = parseInt($(this).attr('id'));
				$('#comment').focus();
			})
			$(document).on('click','#comment', function(){
				comment_id = 0;
				sub_comment = false;
			});
			$(document).on('click','.btn-submit',function(){
				var flag = true;
				var id = parseInt($(this).attr('id'));
				var check = '{{Auth::check()}}';
				var content = $('#comment').val();
				if(check == ''){
					$('#error').text('Vui lòng đăng nhập trước khi bình luận ');
					flag = false
				}
				else if(content ==''){
					$('#error').text('Vui lòng bình luận');
					flag = false
				}
				if(flag){
					if(sub_comment){
						$.ajax({
							type: 'POST',
							url: "{{url('blog/list/'.$blog->id.'/sub_comment')}}",
							data: {
								'content': content,
								'comment_id' : comment_id
							},
							success:function(data){
			      				console.log(data.comment);
			      				sub_commment(data);
			   				}
						})
					}
					else {
						$.ajax({
							type: 'POST',
							url: "{{url('blog/list/'.$blog->id.'/comment')}}",
							data: {
								'content': content,
							},
							success:function(data){
			      				console.log(data.comment);
			      				comment(data);
			   				}
						})
					}
				}

			})
			//Rate:
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );
			$('.ratings_stars').click(function(){
				var flag = true;
				var rate =  $(this).find("input").val();
		    	if ($(this).hasClass('ratings_over')) {
		            $('.ratings_stars').removeClass('ratings_over');
		            $(this).prevAll().andSelf().addClass('ratings_over');
		        } else {
		        	$(this).prevAll().andSelf().addClass('ratings_over');
		        }
		        var check = '{{Auth::check()}}';
		        if(check == ''){
		        	$('#error_rate').text('Vui lòng đăng nhập trước khi rating ');
					flag = false
		        }
		        console.log(flag);
		        if(flag){
		        	$.ajax({
		        		type: 'POST',
		        		url: "{{url('blog/list/'.$blog->id.'/rating')}}",
		        		data: {
		        			'rate' : rate,
		        		},
		        		success:function(data){
			      			var avg_rate = data.avg_rate;
			      			var count_rate = data.count_rate;
			      			console.log(avg_rate);
			      			$("#show-count").text("(" + count_rate + " votes )");
			      			var html ='';
			      			for(var i=1; i<= 5; i++){
			      				if(avg_rate >= i){
			      					html += "<i class='fa fa-star color'></i>";
			      				}
			      				else {
			      					html += "<i class='fa fa-star'></i>";
			      				}
			      			}
			      			$("#show-rate").html(html);
			   			}
		        	})
		        }

		    });
		})
	</script>
@endsection
