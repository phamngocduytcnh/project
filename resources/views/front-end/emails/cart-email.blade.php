<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table {
  		border-collapse: collapse;
  		width: 100%;
  		border:  1px solid groove ;
	}
	th{
		background-color: orange;
		color: white;
		height: 50px;
		font-size: 18px;
		text-align: center;
	}
	th, td {
  		padding: 15px;
  		text-align: center;
	}
	tr:nth-child(even) {background-color: #f2f2f2;}
	.total {
		float: right;

	}
	#total {
		color: orange;
	}
	ul {
		list-style-type: none;
		margin-right: 50px;
	}
	li {
		padding: 10px;
	}
	span{
		margin-left: 20px;
	}
</style>
<body>
	<div style="overflow-x:auto;">
		<table>
			<thead>
				<tr>
					<th>Item</th>
					<th>Product</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($cart as $item)
					<tr>
						<td>
							<img src="{{$message->embed('uploads/products/'.$item['dir'].'/'.$item['img'].'')}}" alt="">
						</td>
						<td>{{ $item['name'] }}</td>
						<td>{{ $item['price'] }}</td>
						<td>{{ $item['quanity'] }}</td>
						<td>{{ $item['price'] * $item['quanity'] }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="total">
			<ul>
				<li>Cart Sub Total <span></span></li>
				<li>Eco Tax <span></span></li>
				<li>Shipping Cost <span>Free</span></li> <hr>
				<li>Total  <span id="total">$ <b>{{ $total }}</b></span></li>
			</ul>
		</div>
	</div>
</body>
</html>