@extends('admin.layouts.main')
@section('content')
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">List of Blog</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Blogs </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session('success'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{{session('success')}}</li>
                                        </ul>
                                    </div>
                                @endif
                                @if(session('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{session('error')}}</li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <div class="table-responsive">
                            	<table class="table">
                            		<thead>
                            			<tr>
                            				<th scope="col">#</th>
                            				<th scope="col">Title</th>
                            				<th scope="col">Description</th>
                            				<th scope="col">Content</th>
                                            <th scope="col">Author</th>
                                            <th scope="col">Action</th>
                                            <th scope="col">Action</th>
                                            <th scope="col">Action</th>
                                            <th scope="col"></th>
                            			</tr>
                            		</thead>
                            		<tbody>
                            			@foreach($blogs as $blog)
	                            			<tr>
                                				<td scope="row">{{$blog->id}}</td>
                                				<td> {{ $blog->title }}</td>
                                                <td> {{ $blog->description }}</td>
                                                <td> {{ htmlspecialchars_decode($blog->content) }} </td>
                                                <td> {{ $blog->User['name']}} </td>
                                				<td> <a href="{{url('/admin/blog/edit/'.$blog->id)}}" class="btn btn-info">Edit</a> </td>
                                                <td> <a href="{{url('/admin/blog/delete/'.$blog->id)}}" class="btn btn-danger"> Delete</a> </td>
	                        				</tr>
                            			@endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><a href="{{route('add-blog')}}" class="btn btn-success" >Add new</a></td>
                                        </tr>
                            		</tbody>
                            	</table>
                                {{ $blogs->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
@endsection