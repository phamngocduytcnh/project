@extends('admin.layouts.main')
@section('content')
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Country</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Country </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	<form class="form-horizontal form-material" method="post">
                            		@csrf
                            		@if ($errors->any())
	                                    <div class="alert alert-danger">
	                                        <ul>
	                                            @foreach ($errors->all() as $error)
	                                                <li>{{ $error }}</li>
	                                            @endforeach
	                                        </ul>
	                                    </div>
                                	@endif
                            		<div class="form-group">
                                        <label class="col-md-12">Add Country</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control form-control-line" name="country" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success"> Add Country </button>
                                        </div>
                                    </div>
                            	</form>
                            </div>
                            <div class="table-responsive">
                            	<table class="table">
                            		<thead>
                            			<tr>
                            				<th scope="col">#</th>
                            				<th scope="col">Country</th>
                            				<th scope="col">Edit</th>
                            				<th scope="col">Delete</th>
                            			</tr>
                            		</thead>
                            		<tbody>
                            			@foreach($countrys as $country)
	                            			<tr>
	                            				<th scope="row">{{$country->id}}</th>
	                            				<td>{{$country->country}}</td>
	                            				<td><i class="m-r-10 mdi mdi-box-cutter"></i></td>
	                            				<td><i class="m-r-10 mdi mdi-close-box"></i></td>
	                        				</tr>
                            			@endforeach
                            		</tbody>
                            		
                            	</table>
                            	{{ $countrys->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
@endsection