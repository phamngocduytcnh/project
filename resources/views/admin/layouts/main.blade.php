@include('admin.layouts.header');
@include('admin.layouts.left-menu');
@yield('content');
@include('admin.layouts.footer');