$(document).ready(function(){
	
	// Show Images before Upload Image
	function readURLImage(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e) {
		      $('#showImg').attr('src', e.target.result);
		    }
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#uploadImg").change(function(){
		readURLImage(this);
	})
})