<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->float('price')->unsigned();
            $table->json('img');
            $table->BigInteger('member_id')->unsigned();
            $table->BigInteger('category_id')->unsigned();
            $table->BigInteger('brand_id')->unsigned();
            $table->BigInteger('state_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categorys');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('state_id')->references('id')->on('states');
            $table->float('sale_off')->unsigned()->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
