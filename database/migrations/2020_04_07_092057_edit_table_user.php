<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ( $table) {
            $table->string('phone')->default('123 456 789') -> after('password');
            $table->string('img')->default('default.png') -> after('password');
            $table->string('message') -> nullable() -> after('password');
            $table->BigInteger('countryId')->default(1)->unsigned()->after('password');
            $table->integer('level')->after('password')->comment('1:admin, 0:member');
            $table->foreign('countryId')->references('id')->on('countrys');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
