<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class Brands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
        	'brand' => 'Da Lat farm'
        ]);
        DB::table('brands')->insert([
        	'brand' => 'Highland Coffee'
        ]);
    }
}
