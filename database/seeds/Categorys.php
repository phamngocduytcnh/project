<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Categorys extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorys')->insert([
        	'category' => 'Nhap khau'
        ]);
        DB::table('categorys')->insert([
        	'category' => 'Xuat khau'
        ]);
    }
}
