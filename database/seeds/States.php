<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class States extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
        	'state' => 'New'
        ]);
        DB::table('states')->insert([
        	'state' => 'Sale'
        ]);
    }
}
